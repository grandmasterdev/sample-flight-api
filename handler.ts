import { ApolloServer, Config, makeExecutableSchema } from 'apollo-server-lambda';
import { importSchema } from 'graphql-import';
import lambdaPlayground from 'graphql-playground-middleware-lambda';
import 'source-map-support/register';
import rootResolver from './resolvers/rootResolver';

const schemas = importSchema('./schemas/rootSchema.graphql');
const exeSchemas = makeExecutableSchema({
  typeDefs: schemas,
  resolvers: rootResolver
})
const serverConfig: Config = {
  schema: exeSchemas
}
const server: ApolloServer = new ApolloServer(serverConfig);

export const graphqlHandler = server.createHandler();

export const playgroundHandler = lambdaPlayground({
  endpoint: '/graphql'
});
