import { injectable } from 'inversify';
import 'reflect-metadata';
import * as request from 'request-promise';
import { RapidFlightServiceInterface } from './interfaces/RapidFlightServiceInterface';
import RapidFlightRequestModel from './../models/RapidFlightRequestModel';
import ResponseModel from './../models/ResponseModel';
import RapidFlightHelper from './../helpers/RapidFlightHelper';

const rapidFlightApiConfig: any = require('./../configs/rapid-flight-api-config.json');

@injectable()
export default class RapidFlightService implements RapidFlightServiceInterface {
    private apiHost: string = '';
    private apiHostPoll: string = '';
    private rapidFlightReqModel: RapidFlightRequestModel;
    private serviceResponse: ResponseModel;

    constructor() {
        this.apiHost = rapidFlightApiConfig.apiHost || '';
        this.apiHostPoll = rapidFlightApiConfig.apiHostPoll || '';
        this.rapidFlightReqModel = new RapidFlightRequestModel();
        this.serviceResponse = new ResponseModel();
    }

    private async pollData(sessionId: string) {
        const pollDataResponse = new ResponseModel();

        if (sessionId) {
            const response: any = await request.get(`${this.apiHostPoll}/${sessionId}`, {
                headers: this.rapidFlightReqModel.headers,
                resolveWithFullResponse: true
            });

            if (response && response.body) {
                pollDataResponse.statusCode = 200;
                pollDataResponse.body = JSON.parse(response.body);
            }
        }else {
            pollDataResponse.statusCode = 400;
            pollDataResponse.body = {
                message: 'Missing session Id'
            }
        }

        return pollDataResponse;
    }

    public async getSession(originPlace: string, destinationPlace: string, outboundDate?: string, cabinClass: string = 'business',) {
        
        // Set parameters 
        this.rapidFlightReqModel.body.adults = 1;
        this.rapidFlightReqModel.body.cabinClass = cabinClass;
        this.rapidFlightReqModel.body.outboundDate = outboundDate || '';
        this.rapidFlightReqModel.body.originPlace = originPlace;
        this.rapidFlightReqModel.body.destinationPlace = destinationPlace;
        this.rapidFlightReqModel.body.outboundDate = outboundDate || '';

        const response: any = await request.post(`${this.apiHost}`, {
            headers: this.rapidFlightReqModel.headers,
            form: this.rapidFlightReqModel.body,
            resolveWithFullResponse: true
        });

        if (response && response.headers && response.headers.location) {
            this.serviceResponse.statusCode = 200;
            this.serviceResponse.body = {
                sessionId: RapidFlightHelper.getSessionIdFromUrl(response.headers.location)
            }
        }

        return this.serviceResponse;
    }

    public async pollSessionResult(sessionId: string) {
        if (sessionId) {
            let keepPolling: boolean = true;

            while(keepPolling) {
                const response: ResponseModel = await this.pollData(sessionId);

                if (response && response.body && response.body.Status && response.body.Status === 'UpdatesComplete') {
                    keepPolling = false;
                    this.serviceResponse = response;

                    break;
                }
            }
            
        }else {
            this.serviceResponse.statusCode = 400;
            this.serviceResponse.body = {
                message: 'Missing session Id'
            }
        }

        return this.serviceResponse;
    }
}