export interface RapidFlightServiceInterface {
    getSession(originPlace: string, destinationPlace: string, outboundDate?: string, cabinClass?: string): Promise<any>;
    pollSessionResult(sessionId: string): Promise<any>;
}