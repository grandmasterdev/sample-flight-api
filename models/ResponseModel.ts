export default class ResponseModel {
    public statusCode: number = 500;
    public body: any = {
        message: 'Internal server error'
    }
}