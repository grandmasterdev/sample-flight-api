import RequestModel from './RequestModel';

export default class RapidFlightRequestModel extends RequestModel {
    public headers: any = {
        'X-RapidAPI-Key': '4cee251176msh0ca4d4bd8ef19f9p1aef58jsn7568f1d1c20b'
    }
    public body: RapidFlightRequestBody = new RapidFlightRequestBody();
}

class RapidFlightRequestBody {
    //  Required
    public country: string = 'US';
    public currency: string = 'USD';
    public locale: string = 'en-US';
    public originPlace: string = 'SFO-sky';
    public destinationPlace: string = 'LHR-sky';
    public outboundDate: string = '';
    public adults: number = 1;

    // Optional
    public inboundDate?: string;
    public cabinClass?: string; // premiumeconomy, business or first
    public children?: number;
    public infants?: number;
    public includeCarriers?: string;
    public groupPricing: string; // true or false
    
}
