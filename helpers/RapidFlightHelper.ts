import * as _ from 'lodash';

export default class RapidFlightHelper {
    public static getSessionIdFromUrl(url: string): string {
        if (url) {
            const sections: any[] = url.split('/');

            if (sections && Array.isArray(sections)) {
                return sections[sections.length - 1];
            }
        }

        return '';
    }

    public static getCheapestPrice(pricingOptions: any[]): number {
        if (pricingOptions) {
            let cheapestPrice: number;

            pricingOptions.map((pricingOption: any) => {
                if (!cheapestPrice) {
                    cheapestPrice = pricingOption.Price
                } else {
                    if (cheapestPrice > pricingOption.Price) {
                        cheapestPrice = pricingOption.Price;
                    }
                }
            })

            return cheapestPrice;
        }

        return 0;
    }

    public static getCheapestPriceOfTheDay(Itineraries: any): any {
        if (Itineraries) {
            let cheapestItinerary: any = null;

            Itineraries.map((itinerary: any) => {
                if(cheapestItinerary === null) {
                    cheapestItinerary = itinerary;
                }else{
                    if(cheapestItinerary.CheapestPrice > itinerary.CheapestPrice) {
                        cheapestItinerary = itinerary;
                    }
                }
            })

            return cheapestItinerary;
        }

        return {};
    }

    public static getAgentsDetail(pricingOptions: any[], agents: any[]): any {
        let agentsDetail: any[] = [];

        if (pricingOptions && agents) {
            pricingOptions.map((pricingOption: any) => {
                pricingOption.Agents.map((pricingAgent: any) => {
                    const search: any = {
                        "Id": pricingAgent
                    }
                    const agent: any = _.find(agents, search);

                    agentsDetail.push(agent);
                })
            })
        }

        return agentsDetail;
    }
}