import * as moment from 'moment';

export default class DateHelper {
    public static daysInMonth(month: number, year: number) {
        return new Date(year, month, 0).getDate();
    }

    public static balanceDayInMonth(currentDay: number, currentMonth: number, currentYear: number) {
        if (currentDay) {
            const totalDays: number = DateHelper.daysInMonth(currentMonth, currentYear);

            return totalDays - currentDay;
        }

        return 0;
    }

    public static getAllDatesInAMonth(currentDate: Date) {
        const dates: string[] = [];
        const day: number = parseInt(moment(currentDate).format('DD'));
        const month: number = parseInt(moment(currentDate).format('MM'));
        const year: number = parseInt(moment(currentDate).format('YYYY'));

        const balanceDays: number = DateHelper.balanceDayInMonth(day, month, year);
        console.log('Balance Days', balanceDays);

        for (let i: number = 0; i <= balanceDays; i++) {
            dates.push(`${year.toString()}-${DateHelper.parseDateMonthToString(month)}-${DateHelper.parseDateMonthToString(day + i)}`);
        }

        if (balanceDays < 30) {
            const nextMonthdate: any = moment(currentDate).add(30, 'days');
            console.log('Next month date', nextMonthdate);
            const day: number = parseInt(moment(nextMonthdate).format('DD'));
            const month: number = parseInt(moment(nextMonthdate).format('MM'));
            const year: number = parseInt(moment(nextMonthdate).format('YYYY'));

            for (let i: number = 1; i <= day; i++) {
                dates.push(`${year.toString()}-${DateHelper.parseDateMonthToString(month)}-${DateHelper.parseDateMonthToString(i)}`);
            }
        }

        return dates;
    }

    public static parseDateMonthToString(val: number) {
        if(val) {
            if(val < 10){
                return `0${val.toString()}`
            } else {
                return val.toString();
            }
        }

        return '';
    }

    public static formatToRapidApiDate(date: Date) {
        if (date) {
            const formattedDate: string = moment(date).format('YYYY-MM-DD');

            return formattedDate;
        }

        return '';
    }
}