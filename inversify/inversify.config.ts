import { Container } from 'inversify';
import { RapidFlightServiceInterface } from '../services/interfaces/RapidFlightServiceInterface';
import RapidFlightService from '../services/RapidFlightService';

const container: Container = new Container();

container.bind<RapidFlightServiceInterface>('RapidFlightServiceInterface').to(RapidFlightService);

export default container;