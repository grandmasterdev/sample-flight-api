This project was bootstrapped with [Serverless Framework](https://serverless.com).

## Available Scripts

In the project directory, you can run:

### `serverless offline start`

Runs the app in the development mode.<br>
Open [http://localhost:4000/playground](http://localhost:4000/playground) to view it in the browser.

The above will give you the documentation of the API (schema and all) so that you can play 
around and learn about the contract.