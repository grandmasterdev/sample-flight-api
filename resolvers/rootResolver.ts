import container from './../inversify/inversify.config';
import { RapidFlightServiceInterface } from './../services/interfaces/RapidFlightServiceInterface';
import ResponseModel from './../models/ResponseModel';
import RapidFlightHelper from '../helpers/RapidFlightHelper';
import DateHelper from '../helpers/DateHelper';

const rapidFlightService: RapidFlightServiceInterface = container.get('RapidFlightServiceInterface');

const rootResolver = {
    Query: {
        search: async (_: any, args: any) => {
            console.log('Searching...', args);
            // Get session Id to poll for result
            const dateCollection: any = DateHelper.getAllDatesInAMonth(new Date());

            let flights: any[] = [];
            let sessionKey: string = '';

            if (dateCollection && Array.isArray(dateCollection)) {
                await Promise.all(dateCollection.map(async (date: string, index: number) => {
                    const getSessionIdResponse: ResponseModel = await rapidFlightService.getSession(args.originPlace, args.destinationPlace, date, 'business');
                    
                    console.log('date', date);

                    if (getSessionIdResponse && getSessionIdResponse.statusCode === 200) {
                        const getFlightDetail: ResponseModel = await rapidFlightService.pollSessionResult(getSessionIdResponse.body.sessionId);

                        if (getFlightDetail && getFlightDetail.body) {
                            getFlightDetail.body.Requested = getFlightDetail.body.Query;

                            // Mutate itinerary
                            const Itineraries: any[] = getFlightDetail.body.Itineraries;

                            Itineraries.map((Itinerary: any) => {
                                const cheapestPrice = RapidFlightHelper.getCheapestPrice(Itinerary.PricingOptions);

                                Itinerary.CheapestPrice = cheapestPrice;
                                Itinerary.Agents = RapidFlightHelper.getAgentsDetail(Itinerary.PricingOptions, getFlightDetail.body.Agents);
                            })

                            // Mutate outbound date 
                            getFlightDetail.body.OutboundDate = date;
                            getFlightDetail.body.CheapestOfTheDay = RapidFlightHelper.getCheapestPriceOfTheDay(Itineraries);

                            flights[index] = getFlightDetail.body;
                            sessionKey = getFlightDetail.body.SessionKey;
                        }
                    }
                })).catch((reason: any) => {
                    console.log('reason', reason);
                }).finally(() => {
                    console.log('finally return response', flights);
                    return {
                        SessionKey: sessionKey,
                        Flights: flights
                    };
                })
            }

            return {
                SessionKey: sessionKey,
                Flights: flights
            }; 
        }
    }
}

export default rootResolver;